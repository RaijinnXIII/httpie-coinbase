"""
Coinbase authorization plugin for HTTPie

Note: Does not support OAuth
"""
import configparser
import json
import hashlib
import hmac
import os
import requests
import time

from httpie.plugins import AuthPlugin
from requests.auth import AuthBase
from requests import Request

__version__ = '1.0.0'
__author__ = 'Ryan Ferguson'
__license__ = 'GNU Public License v3'

COINBASE_API_URL = 'https://api.coinbase.com/v2/'
ACCESS_KEY = 'CB-ACCESS-KEY'
SECRET_KEY = 'CB-SECRET-KEY'


class CoinbaseAuth(AuthBase):

    def __init__(self, api_key: str, api_secret: str):
        self.api_key = api_key
        self.api_secret = api_secret

    def __call__(self, request: Request):
        timestamp = str(int(time.time()))
        message = timestamp + request.method + request.path_url + (request.data or '')
        signature = hmac.new(self.api_secret, message, hashlib.sha256).hexdigest()

        request.headers.update({
            'CB-ACCESS-SIGN': signature,
            'CB-ACCESS-TIMESTAMP': timestamp,
            'CB-ACCESS-KEY': self.api_key,
        })
        return request


class HTTPieCoinbasePlugin(AuthPlugin):

    name = 'Coinbase Auth'
    auth_type = 'coinbase'
    description = 'API Key Authentication for Coinbase'
    auth_require = True
    auth_parse = True
    prompt_password = True

    def get_auth(self, username: str, password: str):
        """
        Override AuthPlugin.get_base() method
        """
        access_key = os.environ.get(ACCESS_KEY) if username is None else username
        secret = os.environ.get(SECRET_KEY) if password is None else password

        auth = CoinbaseAuth(
            api_key=access_key,
            api_secret=secret
        )

        return auth
